import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CameraComponent } from './camera/camera.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [CameraComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CameraComponent]
})
export class ComponentModule { }
