import { Component } from '@angular/core';
import { StatusBar } from '@capacitor/status-bar';
import { Platform } from '@ionic/angular';
import { CacheService } from 'ionic-cache';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [

    { title: 'Login', url: 'login', icon: 'log-in' },
    { title: 'Chat', url: 'chat', icon: 'chatbox' },
    { title: 'Test', url: 'test', icon: 'star' },
    { title: 'Test2', url: 'test2', icon: 'airplane' },
  ];
  darkMode = true;

  constructor(
    private platform: Platform,
    private cache: CacheService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.cache.setDefaultTTL(60 * 60 * 24 * 365);
      this.cache.setOfflineInvalidate(false);
      const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
      this.darkMode = prefersDark.matches;
    });
  }

  useDarkTheme() {
    console.log('usedarkTheme Called with darktheme:' + this.darkMode);

    document.body.classList.toggle('dark');
    this.darkMode = !this.darkMode;
  }

};
