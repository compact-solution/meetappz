importScripts('ngsw-worker.js');
importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

if (workbox) {
  console.log('Workbox is loaded');
} else {
  console.log('Workbox did not load');
}

workbox.setConfig({
  debug: false
});

// registering stale-while-revalidate strategy
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.StaleWhileRevalidate()
);

// registering caching strategy
workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg)$/,
  new workbox.strategies.CacheFirst({
    cacheName: "images",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 60 * 60 // 30 Days
      })
    ]
  })
);

workbox.precaching.precacheAndRoute([]);

workbox.precaching.addPlugins([
  new workbox.broadcastUpdate.Plugin('precache-channel')
])

const updateChannel = new BroadcastChannel('precache-channel');
updateChannel.addEventListener('message', event => {
  if (confirm(`New content is available!. Click OK to refresh`)) {
    document.getElementById('update').style.color = "green";
    document.getElementById('update').innerHTML = 'C-Numbers has been updated to the latest Version.';
    window.location.reload();
  }
});
